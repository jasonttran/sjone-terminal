"""
Serial Interface
"""

from contextlib import contextmanager

from serial import Serial
from serial.serialutil import SerialException

__author__ = "jtran"
__version__ = "1.1.0"


class SerialInterface(object):
    AVAILABLE_BAUDRATES = Serial.BAUDRATES

    def __init__(self, port=None, baud=9600):
        try:
            self._serial = Serial(port=port, baudrate=baud)
        except SerialException:
            self._serial = Serial(port=None, baudrate=baud)

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    """
    Public methods
    """
    def open(self, port=None, baud=None):
        if port:
            self._serial.port = port
        if baud:
            self._serial.baudrate = baud
        try:
            self._serial.open()
            error = 0
        except SerialException:
            error = 1
        return error

    def close(self):
        self._serial.close()

    def read(self):
        message = ""
        if self._serial.is_open:
            message = self._serial.read(size=self._serial.in_waiting)
        return message

    def write(self, data):
        self._serial.write(data)

    """
    Class methods
    """
    @classmethod
    @contextmanager
    def context(cls, port):
        new_obj = None
        try:
            new_obj = cls()
            new_obj.open(port)
            yield new_obj
        finally:
            if new_obj:
                new_obj.close()

    """
    Accessors
    """
    @property
    def serial(self):
        return self._serial

    @property
    def available_bauds(self):
        return list(self._serial.BAUDRATES)

    @property
    def is_open(self):
        return self._serial.is_open




if __name__ == "__main__":  # Test
    pass
