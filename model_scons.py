"""
Model - SCons
"""

import logging
import os
from Queue import Queue, Full, Empty
import subprocess
import sys
from threading import Thread
import time

__author__ = "jtran"
__version__ = "2.0.0"

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class Scons(Thread):
    def __init__(self, sconstruct_dirpath=None, jobs=1, logger=None):
        super(Scons, self).__init__()
        self.setDaemon(True)
        self._sconstruct_dirpath = sconstruct_dirpath
        self._jobs = int(jobs)

        self._build_queue = Queue(maxsize=1)
        self._running = False
        self._terminate = False

        self._logger = logger if (logger is not None) else logging.getLogger()

    def __del__(self):
        self.close()

    """
    Public methods
    """
    def start(self):
        super(Scons, self).start()

    def close(self):
        self._terminate = True

    def build(self, project=None):
        try:
            self._build_queue.put(project, block=True, timeout=0.5)
            success = True
        except Full:
            success = False
        return success

    """
    Overwritten methods
    """
    def run(self):
        while not self._terminate:
            self._running = False
            try:
                project = self._build_queue.get(block=True, timeout=1)
                self._running = True
            except Empty:
                continue

            if not self._sconstruct_dirpath:
                self._logger.error("SCons root directory needs to be defined!")
                continue
            elif not os.path.isdir(self._sconstruct_dirpath):
                self._logger.error("Invalid directory: [{}]".format(self._sconstruct_dirpath))
                continue
            else:
                pass

            cmd = [
                "scons",
                "-j", str(self._jobs),
            ]
            if project:
                cmd.append("--project={}".format(project))

            # self._logger.debug("Using command: {}".format(" ".join(cmd)))
            process = subprocess.Popen(cmd, cwd=self._sconstruct_dirpath, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            # TODO: No real-time standard out?
            while True:
                if process.poll() is not None:
                    break
                else:
                    stdout_message = process.stdout.read().rstrip()
                    stderr_message = process.stderr.read().rstrip()
                    if stdout_message:
                        self._logger.info(stdout_message)
                    if stderr_message:
                        self._logger.error(stderr_message)

        self._running = False
        self.is_alive = False

    """
    Accessors
    """
    @property
    def sconstruct_dirpath(self):
        return self._sconstruct_dirpath

    @property
    def is_running(self):
        return self._running or not self._build_queue.empty()

    """
    Mutators
    """
    @sconstruct_dirpath.setter
    def sconstruct_dirpath(self, new_sconstruct_dirpath):
        self._sconstruct_dirpath = new_sconstruct_dirpath




if __name__ == "__main__":  # Test
    scons = Scons(sconstruct_dirpath=r"C:\git\sjone\projects")
    scons.start()
    print scons.build()
    while scons.is_running:
        time.sleep(0.01)
