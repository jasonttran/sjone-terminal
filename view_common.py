"""

"""

import os

from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

__author__ = "jtran"
__version__ = "1.0.3"

FONT_FILEPATH = os.path.join(os.path.dirname(__file__), "RobotoMono-Regular.ttf")


class Console(GridLayout):
    def __init__(self, **kwargs):
        super(Console, self).__init__(**kwargs)
        self.cols = 2
        self.rows = 1
        self._console = TextInput(multiline=True, font_name=FONT_FILEPATH, readonly=True)
        self.add_widget(self._console)

    """
    Public method
    """
    def write(self, text=None):
        if text is None:
            self._console.text = ""
        else:
            self._console.text += text

    def clear(self):
        self.write(None)

    """
    Accessors
    """
    @property
    def console(self):
        return self._console


class Input(GridLayout):
    def __init__(self, **kwargs):
        super(Input, self).__init__(**kwargs)
        self.cols = 1
        self.rows = 1
        self._input = TextInput(multiline=False, font_name=FONT_FILEPATH)
        self.add_widget(self._input)

    """
    Public methods
    """
    def clear(self):
        self._input.text = ""

    """
    Accessors
    """
    @property
    def input(self):
        return self._input

    @property
    def input_string(self):
        return self._input.text