"""
Model - Serial Stream
"""

from threading import Thread
import time

from serial_interface import SerialInterface

__author__ = "jtran"
__version__ = "2.0.0"


class SerialStream(Thread):
    def __init__(self, out_stream, serial_in_stream):
        super(SerialStream, self).__init__()
        self.setDaemon(True)

        assert isinstance(serial_in_stream, SerialInterface)
        self._serial_in_stream = serial_in_stream

        assert hasattr(out_stream, "write")
        self._out_stream = out_stream

        self._running = False
        self._terminate = False
        self._callback = None  # Callback function should a serial stream error occur
        self._pause = False

    def __del__(self):
        self.close()

    """
    Public methods
    """
    def start(self):
        super(SerialStream, self).start()

    def close(self):
        self._terminate = True

    def resume(self):
        self._pause = False

    def pause(self):
        self._pause = True

    """
    Public methods - advanced
    """
    def set_callback(self, callback):
        self._callback = callback

    """
    Overridden methods
    """
    def run(self):
        while not self._terminate:
            self._running = True

            if self._pause:
                time.sleep(0.01)
                continue

            try:
                if not self._serial_in_stream.is_open:
                    time.sleep(0.01)
                    continue

                message = self._serial_in_stream.read()
                if not message:
                    time.sleep(0.01)
                    continue

                try:
                    self._out_stream.write(message)
                except Exception:  # Attempted to write a non-ASCII character? Ignore and continue
                    continue
            except Exception as err:
                if self._callback is not None:
                    self._callback()

        self._running = False
        self.is_alive = False

    """
    Accessors
    """
    @property
    def is_running(self):
        return self._running

    @property
    def is_paused(self):
        return self._pause
