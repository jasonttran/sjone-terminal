"""

"""

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput

from view_common import Console

__author__ = "jtran"
__version__ = "1.1.2"


class HyperloadView(StackLayout):
    def __init__(self, **kwargs):
        super(HyperloadView, self).__init__(**kwargs)
        self.cols = 1
        self._file_entry_view = FileEntryView(size_hint_y=0.1)
        self._flash_button = Button(text="Flash", font_size=14, size_hint_y=0.1)
        self._flash_console = Console(size_hint_y=0.8)
        self.add_widget(self._file_entry_view)
        self.add_widget(self._flash_button)
        self.add_widget(self._flash_console)

        self._hex_chooser = HexChooser()
        self._hex_chooser_popup = Popup(content=self._hex_chooser, title="", auto_dismiss=False)

    """
    Public methods
    """
    def filesystem_browser_open(self):
        self._hex_chooser.target_hex_filepath = None
        self._hex_chooser_popup.open()

    def filesystem_browser_close(self):
        self._hex_chooser_popup.dismiss()
        if self._hex_chooser.target_hex_filepath is not None:
            self._file_entry_view.hex_filepath_input.text = self._hex_chooser.target_hex_filepath

    """
    Private methods
    """
    def _filesystem_browser_close_callback(self, button_obj):
        self.filesystem_browser_close()

    """
    Accessors
    """
    @property
    def target_hex_filepath(self):
        return self._file_entry_view.hex_filepath_input.text

    @property
    def filesystem_browse_button(self):
        return self._file_entry_view.filesystem_browse_button

    @property
    def flash_button(self):
        return self._flash_button

    @property
    def hex_chooser(self):
        return self._hex_chooser

    @property
    def flash_console(self):
        return self._flash_console

    """
    Mutators
    """
    @target_hex_filepath.setter
    def target_hex_filepath(self, new_target_hex_filepath):
        self._file_entry_view.hex_filepath_input.text = new_target_hex_filepath


class FileEntryView(GridLayout):
    def __init__(self, **kwargs):
        super(FileEntryView, self).__init__(**kwargs)
        self.rows = 1
        self._hex_filepath_input = TextInput(multiline=False, size_hint_x=0.9)
        self._filesystem_browse_button = Button(text="Browse", font_size=14, size_hint_x=0.1)
        self.add_widget(self._hex_filepath_input)
        self.add_widget(self._filesystem_browse_button)

    """
    Accessors
    """
    @property
    def hex_filepath_input(self):
        return self._hex_filepath_input

    @property
    def filesystem_browse_button(self):
        return self._filesystem_browse_button


class HexChooser(StackLayout):
    def __init__(self, **kwargs):
        super(HexChooser, self).__init__(**kwargs)
        self.cols = 1
        filters = [
            lambda dirname, filename: filter(lambda ext: (filename.endswith(ext) and not filename.endswith(".sys")), HexChooserListView.FILTER_FILE_EXT),
        ]
        self._file_chooser = HexChooserListView(filters=filters, size_hint_y=0.9)
        self._close_button = Button(text="Close", font_size=14, size_hint_y=0.1)
        self.add_widget(self._file_chooser)
        self.add_widget(self._close_button)

    """
    Public methods
    """
    def set_submit_callback(self, *args, **kwargs):
        self._file_chooser.set_submit_callback(*args, **kwargs)

    """
    Accessors
    """
    @property
    def target_hex_filepath(self):
        return self._file_chooser.target_hex_filepath

    @property
    def dirpath(self):
        return self._file_chooser.path

    @property
    def close_button(self):
        return self._close_button

    """
    Mutators
    """
    @target_hex_filepath.setter
    def target_hex_filepath(self, new_target_hex_filepath):
        self._file_chooser.target_hex_filepath = new_target_hex_filepath

    @dirpath.setter
    def dirpath(self, new_dirpath):
        self._file_chooser.path = new_dirpath


class HexChooserListView(FileChooserListView):
    FILTER_FILE_EXT = ["hex"]

    def __init__(self, **kwargs):
        super(HexChooserListView, self).__init__(**kwargs)
        self.target_hex_filepath = None
        self._submit_callback = None

    def set_submit_callback(self, callback_func):
        self._submit_callback = callback_func

    """
    Overwritten methods
    """
    def on_submit(*args):
        self = args[0]
        filepath = args[1][0]
        self.target_hex_filepath = filepath
        if self._submit_callback is not None:
            self._submit_callback(self)




if __name__ == "__main__":  # Test

    class TestApp(App):
        def build(self):
            self._hyperload_view = HyperloadView()
            self._hyperload_view.filesystem_browse_button.bind(on_release=self._filesystem_browser_open_callback)
            return self._hyperload_view

        def _filesystem_browser_open_callback(self, button_obj):
            self._hyperload_view.filesystem_browser_open()

    TestApp().run()
