"""

"""

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.stacklayout import StackLayout

from view_common import Console, Input
from view_hyperload import HyperloadView
from view_scons import SconsView
from view_serial_config import SerialConfigurationPopup

import __init__

__author__ = "jtran"
__version__ = "3.3.0"

TITLE = "SJOne Terminal"
VERSION = __init__.__version__


class Root(App):
    def __init__(self, **kwargs):
        super(Root, self).__init__(**kwargs)
        self.title = "{} v{}".format(TITLE, VERSION)

        self._top_level = TopLevel()

    """
    Public methods
    """
    def run(self):
        super(Root, self).run()

    """
    Overridden methods
    """
    def build(self):
        return self._top_level

    """
    Accessors
    """
    @property
    def top_level(self):
        return self._top_level


class TopLevel(GridLayout):
    def __init__(self, **kwargs):
        super(TopLevel, self).__init__(**kwargs)
        self.rows = 1

        self._control_panel = ControlPanel(size_hint_x=0.2)
        self._panel_wrapper = PanelWrapperView(size_hint_x=0.8)
        self.add_widget(self._control_panel)
        self.add_widget(self._panel_wrapper)

        self._serial_config = SerialConfigurationPopup()
        self._serial_config_popup = Popup(content=self._serial_config, title="Serial Configuration", auto_dismiss=False)

    """
    Public methods
    """
    def instantiate_serial_config_popup(self):
        self._serial_config_popup.open()

    def dismiss_serial_config_popup(self):
        self._serial_config_popup.dismiss()

    """
    Accessors
    """
    @property
    def control_panel(self):
        return self._control_panel

    @property
    def panels(self):
        return self._panel_wrapper.panels

    @property
    def serial_config(self):
        return self._serial_config

    @property
    def standard_out(self):
        return self._panel_wrapper.standard_out


class PanelWrapperView(GridLayout):
    def __init__(self, **kwargs):
        super(PanelWrapperView, self).__init__(**kwargs)
        self.cols = 1
        self._panels = Panels(size_hint_y=0.8)
        self._standard_out = Console(size_hint_y=0.2)
        self.add_widget(self._panels)
        self.add_widget(self._standard_out)

    """
    Accessors
    """
    @property
    def panels(self):
        return self._panels

    @property
    def standard_out(self):
        return self._standard_out


"""
Control panel
"""


class ControlPanel(StackLayout):
    def __init__(self, **kwargs):
        super(ControlPanel, self).__init__(**kwargs)
        self.cols = 1
        self._connect_button = Button(size_hint_y=0.1)
        self._serial_config_button = Button(text="Serial Configuration", size_hint_y=0.1, font_size=14)
        self._clear_console_button = Button(text="Clear Console", size_hint_y=0.1)
        self.add_widget(self._connect_button)
        self.add_widget(self._serial_config_button)
        self.add_widget(self._clear_console_button)

        self.connect_button_mark_active(False)

    """
    Public methods
    """
    def connect_button_mark_active(self, state):
        if state:
            self._connect_button.text = "Disconnect"
        else:
            self._connect_button.text = "Connect"

    """
    Accessors
    """
    @property
    def connect_button(self):
        return self._connect_button

    @property
    def serial_config_button(self):
        return self._serial_config_button

    @property
    def clear_console_button(self):
        return self._clear_console_button


"""
Text panel
"""


class Panels(GridLayout):
    """
    Contains:
    1. Tabs to switch between Text windows
    2. Text window (context)
    """
    def __init__(self, **kwargs):
        super(Panels, self).__init__(**kwargs)
        self.cols = 1

        # Tabs
        self._tabs = Tabs(size_hint_y=0.05)
        self.add_widget(self._tabs)

        # Contexts
        self._serial_console = SerialConsolePanel(size_hint_y=0.95)
        self._hyperload = HyperloadView(size_hint_y=0.95)
        self._scons = SconsView(size_hint_y=0.95)
        self._standard_out = Console(size_hint_y=0.95)

        # Initialize context
        self.add_widget(self._serial_console)

    """
    Public methods
    """
    def change_context_serial_console(self):
        self._reset_widgets()
        self.add_widget(self._serial_console)

    def change_context_hyperload(self):
        self._reset_widgets()
        self.add_widget(self._hyperload)

    def chance_context_scons(self):
        self._reset_widgets()
        self.add_widget(self._scons)

    def change_context_standard_out(self):
        self._reset_widgets()
        self.add_widget(self._standard_out)

    """
    Private methods
    """
    def _reset_widgets(self):
        self.clear_widgets()
        self.add_widget(self._tabs)

    """
    Accessors
    """
    @property
    def tabs(self):
        return self._tabs

    @property
    def serial_console(self):
        return self._serial_console

    @property
    def hyperload(self):
        return self._hyperload

    @property
    def scons(self):
        return self._scons

    @property
    def standard_out(self):
        return self._standard_out


class Tabs(GridLayout):
    def __init__(self, **kwargs):
        super(Tabs, self).__init__(**kwargs)
        self.rows = 1
        self._serial_console_button = Button(text="Serial Console")
        self._hyperload_button = Button(text="Hyperload")
        self._scons_button = Button(text="SCons")
        self._standard_out_button = Button(text="Console")
        self.add_widget(self._serial_console_button)
        self.add_widget(self._hyperload_button)
        # self.add_widget(self._scons_button)  # Hide SCons tab
        # self.add_widget(self._standard_out_button) Hide standard out tab

    """
    Accessors
    """
    @property
    def serial_console_button(self):
        return self._serial_console_button

    @property
    def hyperload_button(self):
        return self._hyperload_button

    @property
    def scons_button(self):
        return self._scons_button

    @property
    def standard_out_button(self):
        return self._standard_out_button


"""
Serial console
"""


class SerialConsolePanel(GridLayout):
    def __init__(self, **kwargs):
        super(SerialConsolePanel, self).__init__(**kwargs)
        self.cols = 1
        self._output = Console()
        self._input = Input(size_hint_y=0.1)
        self.add_widget(self._output)
        self.add_widget(self._input)

    """
    Accessors
    """
    @property
    def output(self):
        return self._output

    @property
    def input(self):
        return self._input




if __name__ == "__main__":  # Test
    _root = Root()
    _root.run()
