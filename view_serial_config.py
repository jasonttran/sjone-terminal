"""
View - Serial Configuration
"""

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.stacklayout import StackLayout

__author__ = "jtran"
__version__ = "1.0.0"


class SerialConfigurationPopup(GridLayout):
    def __init__(self, **kwargs):
        super(SerialConfigurationPopup, self).__init__(**kwargs)
        self.cols = 1
        self.spacing = 10
        self._serial_configuration = SerialConfiguration()
        self._apply_button = Button(text="Apply", size_hint_y=0.1)
        self.add_widget(self._serial_configuration)
        self.add_widget(self._apply_button)

    """
    Accessors
    """
    @property
    def serial_configuration(self):
        return self._serial_configuration

    @property
    def apply_button(self):
        return self._apply_button


class SerialConfiguration(StackLayout):
    DEFAULT_PORT_TEXT = "Select port"
    DEFAULT_BAUD_TEXT = "Select baudrate"

    def __init__(self, **kwargs):
        super(SerialConfiguration, self).__init__(**kwargs)
        self.cols = 2
        self.spacing = 3
        self.orientation = "lr-tb"

        # Port dropdown
        self._port_dropdown = DropDown()
        self._port_dropdown_button = Button(text=self.DEFAULT_PORT_TEXT, size_hint_y=0.1)
        self._port_dropdown_button.bind(on_release=self._port_dropdown.open)
        self._port_dropdown.bind(on_select=lambda instance, x: setattr(self._port_dropdown_button, "text", x))
        self.add_widget(self._port_dropdown_button)

        # Baud dropdown
        self._baud_dropdown = DropDown()
        self._baud_dropdown_button = Button(text=self.DEFAULT_BAUD_TEXT, size_hint_y=0.1)
        self._baud_dropdown_button.bind(on_release=self._baud_dropdown.open)
        self._baud_dropdown.bind(on_select=lambda instance, x: setattr(self._baud_dropdown_button, "text", x))
        self.add_widget(self._baud_dropdown_button)

    """
    Public methods
    """
    def populate_port_dropdown(self, available_ports):
        self._port_dropdown.clear_widgets()
        for available_port in available_ports:
            new_button = Button(text=str(available_port), size_hint_y=None, height=44)
            new_button.bind(on_release=lambda btn: self._port_dropdown.select(btn.text))
            self._port_dropdown.add_widget(new_button)

    def populate_baud_dropdown(self, available_bauds):
        self._baud_dropdown.clear_widgets()
        for available_baud in available_bauds:
            new_button = Button(text=str(available_baud), size_hint_y=None, height=44)
            new_button.bind(on_release=lambda btn: self._baud_dropdown.select(btn.text))
            self._baud_dropdown.add_widget(new_button)

    """
    Accessors
    """
    @property
    def port(self):
        text = self._port_dropdown_button.text
        ret = None if (self.DEFAULT_PORT_TEXT == text) else text
        return ret

    @property
    def baud(self):
        text = self._baud_dropdown_button.text
        ret = None if (self.DEFAULT_BAUD_TEXT == text) else text
        return ret

    """
    Mutators
    """
    @port.setter
    def port(self, new_port):
        text = self.DEFAULT_PORT_TEXT if (new_port is None) else str(new_port)
        self._port_dropdown_button.text = text

    @baud.setter
    def baud(self, new_baud):
        text = self.DEFAULT_BAUD_TEXT if (new_baud is None) else str(new_baud)
        self._baud_dropdown_button.text = text




if __name__ == "__main__":  # Test
    _popup = Popup(content=SerialConfigurationPopup(), title="Serial Configuration", auto_dismiss=False)
    _popup.open()
    App().run()
