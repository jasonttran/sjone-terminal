"""

"""

from abc import ABCMeta, abstractmethod, abstractproperty
import glob
import sys

from serial import Serial, SerialException

__author__ = "jtran"
__version__ = "1.0.0"


"""
Public functions
"""


def get_serial_ports():
    platform_specific_serial_ports = [
        SerialPortsWin(),
        SerialPortsLinux(),
        SerialPortsDarwin(),
    ]

    current_platform = sys.platform.lower()

    for serial_ports in platform_specific_serial_ports:
        if filter(lambda platform: current_platform.startswith(platform.lower()), serial_ports.supported_platforms):
            available_serial_ports = serial_ports.available_ports()
            break
    else:
        raise EnvironmentError("Unsupported platform: [{}]".format(current_platform))
    return available_serial_ports


"""
Abstract definitions
"""


class abstractclassmethod(classmethod):

    __isabstractmethod__ = True

    def __init__(self, callable):
        callable.__isabstractmethod__ = True
        super(abstractclassmethod, self).__init__(callable)


class SerialPortsABC(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    """
    Private methods
    """
    @staticmethod
    def get_available_ports(ports):
        """
        Identify available ports by attempting to open all ports
        :param ports: A list of possible ports to try (list of str)
        :return: A list of available ports (list of str)
        """
        valid_ports = []
        for port in ports:
            try:
                serial = Serial(port)
                serial.close()
                valid_ports.append(port)
            except (OSError, SerialException):
                pass
        return valid_ports

    @abstractclassmethod
    def available_ports(cls):
        """
        :return: A list of serial ports (list of str)
        """
        raise NotImplementedError

    @abstractproperty
    def supported_platforms(self):
        """
        :return: A list of supported platforms (list of str)
        """
        raise NotImplementedError


"""
Platform specific serial ports
"""


class SerialPortsWin(SerialPortsABC):
    @classmethod
    def available_ports(cls):
        ports = ['COM%s' % (i + 1) for i in range(256)]
        return cls.get_available_ports(ports)

    @property
    def supported_platforms(self):
        return ["win"]


class SerialPortsLinux(SerialPortsABC):
    @classmethod
    def available_ports(cls):
        ports = glob.glob('/dev/tty[A-Za-z]*')
        return cls.get_available_ports(ports)

    @property
    def supported_platforms(self):
        return ["linux", "cygwin"]


class SerialPortsDarwin(SerialPortsABC):
    @classmethod
    def available_ports(cls):
        ports = glob.glob('/dev/tty.*')
        return cls.get_available_ports(ports)

    @property
    def supported_platforms(self):
        return ["darwin"]




if __name__ == "__main__":  # Test
    print get_serial_ports()
