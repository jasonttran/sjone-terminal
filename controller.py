"""
Controller
"""

import logging
import multiprocessing

from kivy.clock import Clock

from model_config import Configuration
from model_hyperload import HyperloadWrapper
from model_scons import Scons
from model_serial_stream import SerialStream
from model_sjone_interface import SJOneInterface
import serial_ports
from view import Root

__author__ = "jtran"
__version__ = "1.4.0"


class Controller(object):
    def __init__(self):
        self._root = Root()

        self._control_panel = self._root.top_level.control_panel
        self._panels = self._root.top_level.panels
        self._standard_out = self._root.top_level.standard_out
        self._serial_config_view = self._root.top_level.serial_config

        # Binding
        self._control_panel.connect_button.bind(on_release=self._connect_button_callback)
        self._control_panel.clear_console_button.bind(on_release=self._clear_console_button_callback)
        self._panels.serial_console.input.input.bind(on_text_validate=self._input_enter_callback)
        self._panels.tabs.serial_console_button.bind(on_release=self._change_context_serial_console)
        self._panels.tabs.hyperload_button.bind(on_release=self._change_context_hyperload)
        self._panels.tabs.scons_button.bind(on_release=self._change_context_scons)
        self._panels.tabs.standard_out_button.bind(on_release=self._change_context_standard_output)

        # Hyperload
        self._panels.hyperload.filesystem_browse_button.bind(on_release=self._browse_callback)
        self._panels.hyperload.hex_chooser.close_button.bind(on_release=self._close_callback)
        self._panels.hyperload.hex_chooser.set_submit_callback(self._close_callback)
        self._panels.hyperload.flash_button.bind(on_release=self._flash_callback)

        # SCons
        self._panels.scons.build_button.bind(on_release=self._build_callback)

        # Serial configuration popup
        self._control_panel.serial_config_button.bind(on_release=self._instantiate_serial_config_popup_callback)
        self._serial_config_view.apply_button.bind(on_release=self._apply_serial_config_popup_callback)
        self._serial_config_view.serial_configuration.populate_baud_dropdown(SJOneInterface.AVAILABLE_BAUDRATES)
        self._serial_config_view.serial_configuration.baud = SJOneInterface.DEFAULT_BAUD  # Initialize default baud for SJOne

        # Saved configuration
        if Configuration.is_settings_valid():
            self._configuration = Configuration.from_json()
            self._init_config_view()
        else:
            self._configuration = Configuration()

        # Serial initialization
        self._sjone_if = SJOneInterface()

        # Serial stream
        self._serial_stream = SerialStream(out_stream=self._panels.serial_console.output, serial_in_stream=self._sjone_if)
        self._serial_stream.set_callback(self._serial_stream_error_callback)

        # Hyperload
        logger = logging.getLogger("hyperload")
        logger.setLevel(logging.DEBUG)
        stream_handler = logging.StreamHandler(stream=self._panels.hyperload.flash_console)
        stream_handler.setFormatter(fmt=logging.Formatter())
        logger.addHandler(stream_handler)
        self._hyperload = HyperloadWrapper(self._sjone_if.serial, logger=logger)
        self._hyperload.set_callback(self._hyperload_callback)
        self._hyperload.start()

        # SCons builder
        logger = logging.getLogger("scons")
        logger.setLevel(logging.DEBUG)
        stream_handler = logging.StreamHandler(stream=self._panels.scons.build_console)
        stream_handler.setFormatter(fmt=logging.Formatter())
        logger.addHandler(stream_handler)
        self._scons = Scons(jobs=multiprocessing.cpu_count()*2, logger=logger)
        self._scons.start()

    """
    Public methods
    """
    def run(self):
        self._serial_stream.start()
        self._root.run()

    """
    Public methods - advanced
    """
    def write(self, data):
        data = str(data)
        self._standard_out.write(data)

    """
    Private methods
    """
    def _serial_stream_error_callback(self):
        self._serial_disconnect()

    """
    Private methods - serial connection
    """
    def _serial_connect(self):
        target_port = self._configuration.port
        target_baud = self._configuration.baud
        if target_port is not None:
            if self._sjone_if.is_open:
                error = self._serial_disconnect()
                if error:
                    return error  # Return early

            error = self._sjone_if.open(port=target_port, baud=target_baud)
            if error:
                print "Unable to connect to serial port: [{}] baud: [{}]".format(target_port, target_baud)
            else:
                print "Connected to serial port: [{}] baud: [{}]".format(target_port, target_baud)
        else:
            print "Target serial port is not defined! Select a serial port in the serial configuration tab."
            error = 1
        self._control_panel.connect_button_mark_active(not error)
        return error

    def _serial_disconnect(self):
        error = 1
        if not self._hyperload.is_running:
            self._sjone_if.close()
            self._control_panel.connect_button_mark_active(False)
            print "Disconnected from serial port: [{}] baud: [{}]".format(self._sjone_if.serial.port, self._sjone_if.serial.baudrate)
            error = 0
        return error

    """
    Private methods - saved configuration
    """
    def _init_config_view(self):
        for attr, value in self._configuration:
            if (attr == "port") and (value is not None) and (value in serial_ports.get_serial_ports()):
                self._serial_config_view.serial_configuration.port = value
            elif (attr == "baud") and (value is not None):
                self._serial_config_view.serial_configuration.baud = value
            elif (attr == "hex_filepath") and (value is not None):
                self._panels.hyperload.target_hex_filepath = value
            elif (attr == "scons_sconstruct_dirpath") and (value is not None):
                self._panels.scons.scons_project_definition.sconstruct_dirpath = value
            elif (attr == "scons_project") and (value is not None):
                self._panels.scons.scons_project_definition.project = value
            else:
                pass

    """
    GUI methods
    """
    def _connect_button_callback(self, button_obj):
        if not self._sjone_if.is_open:
            self._serial_connect()
        else:
            self._serial_disconnect()

    def _clear_console_button_callback(self, button_obj):
        self._panels.serial_console.output.clear()
        self._standard_out.clear()

    def _input_enter_callback(self, textinput_obj):
        input_string = self._panels.serial_console.input.input_string
        Clock.schedule_once(self._focus_text_panel_input)
        if input_string and self._sjone_if.is_open:
            input_string = str(input_string)
            input_string += "\r\n"  # Inject enter
            self._sjone_if.write(input_string)
            self._panels.serial_console.input.clear()

    def _focus_text_panel_input(self, event):
        self._panels.serial_console.input.input.focus = True

    def _instantiate_serial_config_popup_callback(self, button_obj):
        self._serial_config_view.serial_configuration.populate_port_dropdown(serial_ports.get_serial_ports())
        self._root.top_level.instantiate_serial_config_popup()

    def _apply_serial_config_popup_callback(self, button_obj):
        self._root.top_level.dismiss_serial_config_popup()
        original_port = self._configuration.port
        original_baud = self._configuration.baud
        if (original_port != self._serial_config_view.serial_configuration.port) or (original_baud != self._serial_config_view.serial_configuration.baud):
            self._configuration.port = self._serial_config_view.serial_configuration.port
            self._configuration.baud = self._serial_config_view.serial_configuration.baud
            self._configuration.to_json()
            if self._sjone_if.is_open:
                self._panels.serial_console.output.clear()
                self._serial_connect()

    """
    GUI methods - hyperload
    """
    def _browse_callback(self, button_obj):
        self._panels.hyperload.filesystem_browser_open()

    def _close_callback(self, button_obj):
        self._panels.hyperload.filesystem_browser_close()
        self._configuration.hex_filepath = self._panels.hyperload.target_hex_filepath
        self._configuration.to_json()

    def _flash_callback(self, button_obj):
        if not self._hyperload.is_running:
            self._serial_stream.pause()
            if not self._sjone_if.is_open:
                self._serial_connect()
            self._panels.hyperload.flash_console.clear()
            target_hex_filepath = self._panels.hyperload.target_hex_filepath
            self._hyperload.flash(target_hex_filpath=target_hex_filepath)

    def _hyperload_callback(self):
        self._panels.serial_console.output.clear()
        self._serial_stream.resume()
        if (self._hyperload.error is not None) and (not self._hyperload.error):
            self._panels.change_context_serial_console()

    """
    GUI methods - scons
    """
    def _build_callback(self, button_obj):
        sconstruct_dirpath = self._panels.scons.scons_project_definition.sconstruct_dirpath
        project = self._panels.scons.scons_project_definition.project
        self._panels.scons.build_console.clear()
        self._configuration.scons_sconstruct_dirpath = sconstruct_dirpath
        self._configuration.scons_project = project
        self._configuration.to_json()
        self._scons.sconstruct_dirpath = sconstruct_dirpath
        self._scons.build(project=project)  # Ignore return value

    """
    GUI methods - context
    """
    def _change_context_serial_console(self, button_obj):
        self._panels.change_context_serial_console()

    def _change_context_hyperload(self, button_obj):
        self._panels.change_context_hyperload()

    def _change_context_scons(self, button_obj):
        self._panels.chance_context_scons()

    def _change_context_standard_output(self, button_obj):
        self._panels.change_context_standard_out()




if __name__ == "__main__":  # Test
    _controller = Controller()
    _controller.run()
