"""
Model - Saved Configuration
"""

from collections import OrderedDict
import json
import os

__author__ = "jtran"
__version__ = "2.0.0"

DEFAULT_SAVED_SETTINGS_FILEPATH = os.path.join(os.path.dirname(__file__), "saved_configuration.json")


class Configuration(object):
    def __init__(self, port=None, baud=None, hex_filepath=None, scons_sconstruct_dirpath=None, scons_project=None):
        self._port = port
        self._baud = baud
        self._hex_filepath = hex_filepath
        self._scons_sconstruct_dirpath = scons_sconstruct_dirpath
        self._scons_project = scons_project

    def __iter__(self):
        for var_name, var_value in vars(self).items():
            if var_name.startswith("_"):
                var_name = var_name[1:]  # Strip leading underscore
            yield var_name, var_value

    """
    Public methods
    """
    def serialize(self):
        serialized_obj = OrderedDict()
        for var_name, var_value in self:
            serialized_obj[var_name] = var_value
        return serialized_obj

    @classmethod
    def deserialize(cls, dict):
        return cls(**dict)

    def to_json(self, json_filepath=DEFAULT_SAVED_SETTINGS_FILEPATH):
        with open(json_filepath, "w") as file_obj:
            serialized_obj = self.serialize()
            json.dump(serialized_obj, fp=file_obj, indent=4)

    @classmethod
    def from_json(cls, json_filepath=DEFAULT_SAVED_SETTINGS_FILEPATH):
        with open(json_filepath, "r") as file_obj:
            json_dict = json.load(file_obj, object_pairs_hook=OrderedDict)
        return cls.deserialize(json_dict)

    @classmethod
    def is_settings_valid(cls, json_filepath=DEFAULT_SAVED_SETTINGS_FILEPATH):
        try:
            cls.from_json(json_filepath)
        except Exception:
            valid = False
        else:
            valid = True
        return valid

    """
    Accessors
    """
    @property
    def port(self):
        return self._port

    @property
    def baud(self):
        return self._baud

    @property
    def hex_filepath(self):
        return self._hex_filepath

    @property
    def scons_sconstruct_dirpath(self):
        return self._scons_sconstruct_dirpath

    @property
    def scons_project(self):
        return self._scons_project

    """
    Mutators
    """
    @port.setter
    def port(self, new_port):
        self._port = new_port

    @baud.setter
    def baud(self, new_baud):
        self._baud = new_baud

    @hex_filepath.setter
    def hex_filepath(self, new_hex_filepath):
        self._hex_filepath = new_hex_filepath

    @scons_sconstruct_dirpath.setter
    def scons_sconstruct_dirpath(self, new_scons_sconstruct_dirpath):
        self._scons_sconstruct_dirpath = new_scons_sconstruct_dirpath

    @scons_project.setter
    def scons_project(self, new_scons_project):
        self._scons_project = new_scons_project




if __name__ == "__main__":  # Test
    _serial_config = Configuration("COM0", 9600)
    print _serial_config.serialize()
    print Configuration.is_settings_valid()
