"""
Entry point
"""

import os
import sys

temp_file_obj = None
if "pythonw" in os.path.basename(sys.executable):
    temp_file_obj = open(os.path.join(os.path.dirname(__file__), "temp.log"), "w")
    sys.stdout = temp_file_obj
    sys.stderr = temp_file_obj

from controller import Controller

__author__ = "jtran"
__version__ = "1.0.2"

CONSOLE_OUTPUT_FILEPATH = os.path.join(os.path.dirname(__file__), "console.log")


def main():
    terminal_controller = Controller()

    standard_out_wrapper = StandardOutWrapper(terminal_controller, CONSOLE_OUTPUT_FILEPATH)
    sys.stdout = standard_out_wrapper
    sys.stderr = standard_out_wrapper

    terminal_controller.run()
    if temp_file_obj is not None:
        temp_file_obj.close()
    return 0


class StandardOutWrapper(object):
    def __init__(self, terminal_stream, console_filepath):
        self._terminal_stream = terminal_stream
        self._console_filepath = console_filepath
        with open(self._console_filepath, "w") as file_obj:
            pass

    def write(self, data):
        with open(self._console_filepath, "a") as file_obj:
            file_obj.write(data)
        self._terminal_stream.write(data)


if __name__ == "__main__":
    sys.exit(main())
