"""
Model - Hyperload
"""

import os
from Queue import Queue, Full, Empty
from threading import Thread

from hyperload import Hyperload

__author__ = "jtran"
__version__= "1.2.1"


class HyperloadWrapper(Hyperload, Thread):

    class FlashError(Exception):
        pass

    def __init__(self, serial_handle, **kwargs):
        super(Hyperload, self).__init__(**kwargs)  # Call parent's parent constructor (not parent's constructor)
        Thread.__init__(self)
        self.setDaemon(True)
        self._serial = serial_handle

        self._flash_queue = Queue(maxsize=1)
        self._running = False
        self._terminate = False
        self._callback = None
        self._error = None

    def __del__(self):
        self.close()

    """
    Public methods
    """
    def start(self):
        Thread.start(self)

    def close(self):
        self._terminate = True

    def flash(self, target_hex_filpath):
        try:
            self._flash_queue.put(target_hex_filpath, block=True, timeout=0.5)
            success = True
        except Full:
            success = False
        return success

    """
    Public methods - advanced
    """
    def set_callback(self, callback):
        self._callback = callback

    """
    Overwritten methods
    """
    def run(self):
        while not self._terminate:
            self._running = False
            try:
                target_hex_filepath = self._flash_queue.get(block=True, timeout=1)
                self._running = True
            except Empty:
                continue

            try:
                if not self._serial.is_open:
                    raise self.FlashError("Cannot flash - no active serial connection!")
                if not os.path.isfile(target_hex_filepath):
                    raise self.FlashError("File not found: [{}]".format(target_hex_filepath))
                elif (target_hex_filepath is None) or (isinstance(target_hex_filepath, str) and not target_hex_filepath.endswith(".hex")):
                    raise self.FlashError("File is invalid: [{}]".format(target_hex_filepath))
                else:
                    pass

                self._error = None
                original_baud = self._serial.baudrate
                error = 1
                try:
                    for progress, error in self.update_iter(program_filepath=target_hex_filepath):
                        if error:
                            break
                        else:
                            self._logger.info("{}%".format(progress))
                    if not error:
                        self._logger.info("Success!")
                except Exception as err:
                    pass  # TODO: Don't ignore error
                finally:
                    self._serial_change_baud(new_baud=original_baud)
                    self._error = error
            except self.FlashError as err:
                self._logger.error(err)
            except Exception as err:
                self._logger.error(err)
            finally:
                if self._callback is not None:
                    self._callback()

        self._running = False
        self.is_alive = False

    """
    Accessors
    """
    @property
    def is_running(self):
        return self._running

    @property
    def error(self):
        return self._error




if __name__ == "__main__":  # Test
    import serial
    _hyperload_wrapper = HyperloadWrapper(serial_handle=serial.Serial())