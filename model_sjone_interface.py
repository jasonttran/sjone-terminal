"""
Model - SJOne Interface
"""

from Queue import Queue, Empty
from threading import Thread, RLock
import time

from serial_interface import SerialInterface

__author__ = "jtran"
__version__ = "1.1.0"


def critical_section(func):
    def function_wrapper(*args, **kwargs):
        self = args[0]
        self._lock.acquire()
        try:
            ret = func(*args, **kwargs)
        finally:
            self._lock.release()
        return ret
    return function_wrapper


class SJOneInterface(SerialInterface):
    DEFAULT_BAUD = 38400

    def __init__(self, **kwargs):
        kwargs["baud"] = self.DEFAULT_BAUD
        super(SJOneInterface, self).__init__(**kwargs)

        self._handle_in_stream_thread = Thread(target=self._handle_in_stream)
        self._handle_in_stream_thread.setDaemon(True)

        self._handle_out_stream_thread = Thread(target=self._handle_out_stream)
        self._handle_out_stream_thread.setDaemon(True)

        self._in_stream_queue = Queue()
        self._out_stream_queue = Queue()

        #self._handle_in_stream_thread.start()
        #self._handle_out_stream_thread.start()

        self._terminate = False
        self._lock = RLock()

    def __del__(self):
        self._terminate = True

    """
    Public methods
    """
    def perform_reset(self):
        if self.is_open:
            self._serial.rts = True
            self._serial.dtr = True
            time.sleep(0.1)
            self._serial.rts = False
            self._serial.dtr = False

    """
    Overridden methods
    """
    @critical_section
    def open(self, *args, **kwargs):
        error = super(SJOneInterface, self).open(*args, **kwargs)
        if not error:
            self.perform_reset()
        return error

    @critical_section
    def close(self):
        return super(SJOneInterface, self).close()

    @critical_section
    def read(self):
        return super(SJOneInterface, self).read()

    @critical_section
    def write(self, *args, **kwargs):
        return super(SJOneInterface, self).write(*args, **kwargs)

    """
    Threaded methods
    """
    def _handle_in_stream(self):
        while not self._terminate:
            if self.is_open:
                for char in self.read():
                    self._in_stream_queue.put(char)

    def _handle_out_stream(self):
        while not self._terminate:
            try:
                data = self._out_stream_queue.get(block=False, timeout=1)
            except Empty:
                time.sleep(0.001)
                continue
            self.write(data)

    """
    Accessors
    """
    @property
    def in_stream_queue(self):
        return self._in_stream_queue

    @property
    def out_stream_queue(self):
        return self._out_stream_queue




if __name__ == "__main__":  # Test
    import time

    _sjoneif = SJOneInterface()
    _sjoneif.open("COM8")
    print _sjoneif.is_open
    _sjoneif.close()

    with SJOneInterface(port="COM8") as sjoneif:
        print sjoneif.is_open
        sjoneif.write("A")
        time.sleep(1)
        print sjoneif.read()
