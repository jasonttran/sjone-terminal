"""

"""

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput

from view_common import Console

__author__ = "jtran"
__version__ = "1.0.0"


class SconsView(StackLayout):
    def __init__(self, **kwargs):
        super(SconsView, self).__init__(**kwargs)
        self.cols = 1
        self.spacing = 3
        self._scons_project_definition = SconsProjectDefinition(size_hint_y=0.1)
        self._build_button = Button(text="Build", size_hint_y=0.1)
        self._build_console = Console(size_hint_y=0.8)
        self.add_widget(self._scons_project_definition)
        self.add_widget(self._build_button)
        self.add_widget(self._build_console)

    """
    Accessors
    """
    @property
    def scons_project_definition(self):
        return self._scons_project_definition

    @property
    def build_button(self):
        return self._build_button

    @property
    def build_console(self):
        return self._build_console


class SconsProjectDefinition(GridLayout):
    def __init__(self, **kwargs):
        super(SconsProjectDefinition, self).__init__(**kwargs)
        self.rows = 1
        self.spacing = 1
        self._sconstruct_dirpath_text = TextInput(multiline=False, size_hint_x=0.7)
        self._project_text = TextInput(multiline=False, size_hint_x=0.3)
        self.add_widget(self._sconstruct_dirpath_text)
        self.add_widget(self._project_text)

    """
    Accessors
    """
    @property
    def sconstruct_dirpath(self):
        return self._sconstruct_dirpath_text.text

    @property
    def project(self):
        return self._project_text.text

    """
    Mutators
    """
    @sconstruct_dirpath.setter
    def sconstruct_dirpath(self, new_sconstruct_dirpath):
        self._sconstruct_dirpath_text.text = new_sconstruct_dirpath

    @project.setter
    def project(self, new_project):
        self._project_text.text = new_project




if __name__ == "__main__":  # Test

    class TestApp(App):
        def build(self):
            return SconsView()

    TestApp().run()
